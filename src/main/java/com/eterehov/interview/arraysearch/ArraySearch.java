package com.eterehov.interview.arraysearch;

public class ArraySearch {

    public static int findClosest(int[] items, int value) {
        if (items == null || items.length == 0) {
            throw new RuntimeException("Items can not be null or empty");
        }

        if (items.length == 1) {
            return items[0];
        }

        int lowerIndex = 0;
        int upperIndex = items.length - 1;

        if (items[lowerIndex] >= value) {
            return items[lowerIndex];
        }

        if (items[upperIndex] <= value) {
            return items[upperIndex];
        }

        while (upperIndex - lowerIndex > 1) {
            int middleLeftIndex = lowerIndex + (upperIndex - lowerIndex) / 2;
            int middleRightIndex = middleLeftIndex + 1;

            if (items[middleLeftIndex] > value) {
                upperIndex = middleLeftIndex;
            } else if (items[middleRightIndex] < value) {
                lowerIndex = middleLeftIndex;
            } else {
                lowerIndex = middleLeftIndex;
                upperIndex = middleRightIndex;
            }
        }

        int lowerDifference = value - items[lowerIndex];
        int upperDifference = items[upperIndex] - value;

        if (upperDifference < lowerDifference) {
            return items[upperIndex];
        } else {
            return items[lowerIndex];
        }
    }

}

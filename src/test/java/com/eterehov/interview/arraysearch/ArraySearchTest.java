package com.eterehov.interview.arraysearch;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ArraySearchTest {

    @Test(expected = RuntimeException.class)
    public void testFindClosest_NullArray() {
        int[] array = {};

        int result = ArraySearch.findClosest(array, 7);
    }

    @Test(expected = RuntimeException.class)
    public void testFindClosest_ZeroArray() {
        int[] array = {};

        int result = ArraySearch.findClosest(array, 7);
    }

    @Test
    public void testFindClosest_SingleValueArray_EqualsCase() {
        int[] array = {5};

        int result = ArraySearch.findClosest(array, 5);

        assertThat(result, equalTo(5));
    }

    @Test
    public void testFindClosest_SingleValueArray_HigherCase() {
        int[] array = {5};

        int result = ArraySearch.findClosest(array, 7);

        assertThat(result, equalTo(5));
    }

    @Test
    public void testFindClosest_SingleValueArray_LowerCase() {
        int[] array = {5};

        int result = ArraySearch.findClosest(array, 3);

        assertThat(result, equalTo(5));
    }

    @Test
    public void testFindClosest_DoubleValueArray_EqualsLeftCase() {
        int[] array = {3, 6};

        int result = ArraySearch.findClosest(array, 3);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_DoubleValueArray_EqualsRightCase() {
        int[] array = {3, 6};

        int result = ArraySearch.findClosest(array, 6);

        assertThat(result, equalTo(6));
    }

    @Test
    public void testFindClosest_DoubleValueArray_LowerCase() {
        int[] array = {3, 6};

        int result = ArraySearch.findClosest(array, 3);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_DoubleValueArray_UpperCase() {
        int[] array = {3, 6};

        int result = ArraySearch.findClosest(array, 3);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_DoubleValueArray_MiddleCase() {
        int[] array = {3, 7};

        int result = ArraySearch.findClosest(array, 5);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_DoubleValueArray_MiddleLeftCase() {
        int[] array = {3, 7};

        int result = ArraySearch.findClosest(array, 4);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_DoubleValueArray_MiddleRightCase() {
        int[] array = {3, 7};

        int result = ArraySearch.findClosest(array, 6);

        assertThat(result, equalTo(7));
    }

    @Test
    public void testFindClosest_DoubleValueArray() {
        int[] array = {3, 6};

        int result = ArraySearch.findClosest(array, 3);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_MultipleValueArray_EqualsCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 11);

        assertThat(result, equalTo(11));
    }

    @Test
    public void testFindClosest_MultipleValueArray_UpperCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 153);

        assertThat(result, equalTo(23));
    }

    @Test
    public void testFindClosest_MultipleValueArray_LowerCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 0);

        assertThat(result, equalTo(1));
    }

    @Test
    public void testFindClosest_MultipleValueArray_MiddleCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 5);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_MultipleValueArray_MiddleLeftCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 4);

        assertThat(result, equalTo(3));
    }

    @Test
    public void testFindClosest_MultipleValueArray_MiddleRightCase() {
        int[] array = {1, 3, 7, 11, 23};

        int result = ArraySearch.findClosest(array, 6);

        assertThat(result, equalTo(7));
    }

    @Test
    public void testFindClosest_LargeArrayLowerCase() {
        int n = 100000000;
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = i;
        }

        int result = ArraySearch.findClosest(array, array[0] - 1);

        assertThat(result, equalTo(array[0]));
    }

    @Test
    public void testFindClosest_LargeArrayUpperCase() {
        int n = 100000000;
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = i;
        }

        int result = ArraySearch.findClosest(array, array[n - 1] + 1);

        assertThat(result, equalTo(array[n - 1]));
    }

    @Test
    public void testFindClosest_LargeArrayEqualsCase() {
        int n = 100000000;
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = i;
        }

        int result = ArraySearch.findClosest(array, 16);

        assertThat(result, equalTo(16));
    }

}
